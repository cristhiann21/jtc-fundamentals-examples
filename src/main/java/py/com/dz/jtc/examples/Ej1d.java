package py.com.dz.jtc.examples;

/**
 * Clase Ej1d
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej1d {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        //Inicializacion de variables
        String saludo = "Hola ";
        String nombre = "Juan";
        int edad = 25;
        
        //Concatenacion de cadenas y posterior impresion de resultados
        String resultado = saludo + nombre;
        System.out.println(resultado);
        System.out.println(nombre + " tiene actualmente " + edad + " años");
        
        //Impresion de valor inicial
        System.out.println("La inicial del nombre es " + nombre.charAt(0));
    } //Fin de main

} //Fin de clase Ej1d
