package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej4a
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej4a {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int nro;
        for (int i = 0; i <= 100; i++) {
            System.out.print("Ingrese un nro (0 para terminar): ");
            nro = scan.nextInt();
            
            if (nro == 0) {
                break;
            }
        }
        System.out.print("\n");
        scan.close();

    } //Fin de main

} //Fin de clase Ej4a
