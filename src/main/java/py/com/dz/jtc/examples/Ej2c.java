package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej2c
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej2c {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        String saludo = "Hola ";
        String nombre;
        
        Scanner scan = new Scanner(System.in);
        System.out.print("Ingrese su nombre: ");
        nombre = scan.next();
        
        int edad;
        System.out.print("Ingrese su edad: ");
        edad = scan.nextInt();
        
        String resultado = saludo + nombre;
        System.out.println(resultado);
        System.out.println(nombre + " tiene actualmente " + edad + " años");
        
        scan.close();

    } //Fin de main

} //Fin de clase Ej2c
