package py.com.dz.jtc.examples;

/**
 * Clase Ej1a
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej1a {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        String saludo = "Hola ";
        String nombre = "NOMBRE";
        String resultado = saludo + nombre;
        System.out.println(resultado);

    } //Fin de main

} //Fin de clase Ej1a
