package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej3d 
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej3d {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        int base, altura;
        Scanner scan = new Scanner(System.in);

        System.out.print("Ingresa la base del triangulo: ");
        base = scan.nextInt();

        System.out.print("Ingresa la altura del triangulo: ");
        altura = scan.nextInt();

        double superficie = (base * altura) / 2.0;
        System.out.println("La superficie del triangulo con base " + base + " y altura " + altura + " es de " + superficie);
        
        scan.close();

    } //Fin de main
    
} //Fin de clase Ej3d
