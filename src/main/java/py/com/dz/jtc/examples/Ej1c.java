package py.com.dz.jtc.examples;

/**
 * Clase Ej1c
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej1c {

    public static void main(String[] args) {
        String saludo = "Hola ";
        String nombre = "Juan";
        int edad = 25;
        
        String resultado = saludo + nombre;
        System.out.println(resultado);
        System.out.println(nombre + " tiene actualmente " + edad + " años");
        
        System.out.println("La inicial del nombre es " + nombre.charAt(0));

    } //Fin de main

} //Fin de clase Ej1c
