package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej2b
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej2b {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese una cadena: ");
        String cadena = scan.next();
        System.out.println("Ingresaste " + cadena);
        
        System.out.println("Ingrese un nro: ");
        int nro = scan.nextInt();               
        System.out.println("Ingresaste " + nro);
        
        scan.close();
        
    } //Fin de main

} //Fin de clase Ej2b
