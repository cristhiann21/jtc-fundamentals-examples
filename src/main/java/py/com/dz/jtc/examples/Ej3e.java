package py.com.dz.jtc.examples;

/**
 * Clase Ej3e
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej3e {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        System.out.print("Lista de nros. pares de 0 a 100: ");
        for (int i = 0; i <= 100; i++) {
            //Si el resto de dividir el nro i por 2 es cero, entonces es par
            if (i % 2 == 0) {
                System.out.print(" " + i);
            }
        }
        System.out.print("\n");

    } //Fin de main

} //Fin de clase Ej3e
