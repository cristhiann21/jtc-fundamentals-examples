package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej3f
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej3f {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        
        System.out.print("Ingresa el dia de la semana: ");
        int dia = scan.nextInt();
        
        switch (dia) {
            case 1:
                System.out.println("DOMINGO");
                break;
            case 2:                
                System.out.println("LUNES");
                break;
            case 3:   
                System.out.println("MARTES");
                break;
            case 4:
                System.out.println("MIERCOLES");
                break;
            case 5:
                System.out.println("JUEVES");
                break;
            case 6:
                System.out.println("VIERNES");
                break;
            case 7:
                System.out.println("SABADO");
                break;
            default:
                System.out.println("Error de DIA");
                break;
        }  
        
        scan.close();

    } //Fin de main

} //Fin de clase Ej3f
