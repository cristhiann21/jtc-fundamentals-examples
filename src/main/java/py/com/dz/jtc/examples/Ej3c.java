package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej3c 
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej3c {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        int A, B;
        Scanner scan = new Scanner(System.in);

        System.out.print("Ingresa el primer nro: ");
        A = scan.nextInt();

        System.out.print("Ingresa el segundo nro: ");
        B = scan.nextInt();

        if (A > B) {
            System.out.println("El mayor es " + A);
        } else if (B > A) {
            System.out.println("El mayor es " + B);
        } else {
            System.out.println("Los nros son iguales");
        }

        if (A != B) {
            int max = (A > B) ? A : B;
            System.out.println("El maximo entre A y B usando ternario es " + max);
        } else {
            System.out.println("Los nros son iguales");
        }
        
        int suma = A + B;
        System.out.println("La suma es " + suma);
        
        int resta = A - B;
        System.out.println("La resta es " + resta);
        
        int mult = A * B;
        System.out.println("El producto es " + mult);
        
        if (B > 0) {
            int cociente = A / B;
            System.out.println("El cociente entero es " + cociente);
        } else {
            System.out.println("No se puede dividir por cero");
        }
        
        scan.close();

    } //Fin de main
    
} //Fin de clase Ej3c
